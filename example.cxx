#include "vtkm/io/reader/VTKDataSetReader.h"
#include "vtkm/Range.h"
#include "vtkm/filter/Contour.h"
#include "vtkm/Bounds.h"
#include "vtkm/worklet/Normalize.h"
#include "vtkm/rendering/Camera.h"
#include "vtkm/cont/ColorTable.h"
#include "vtkm/rendering/Scene.h"
#include "vtkm/rendering/MapperRayTracer.h"
#include "vtkm/rendering/CanvasRayTracer.h"
#include "vtkm/rendering/Color.h"
#include "vtkm/rendering/Actor.h"
#include "vtkm/rendering/View3D.h"

int main(int argc, char *argv[])
{

  vtkm::io::reader::VTKDataSetReader reader("../../vtk-m-tutorial/data/kitchen.vtk");
  vtkm::cont::DataSet inputData = reader.ReadDataSet();
  std::string fieldName = "v1";

  vtkm::Range range;
  inputData.GetPointField(fieldName).GetRange(&range);
  vtkm::Float64 isovalue = range.Center();

  // Create an isosurface filter
  vtkm::filter::Contour filter;
  filter.SetIsoValue(0, isovalue);
  filter.SetActiveField(fieldName);
  vtkm::cont::DataSet outputData = filter.Execute(inputData);

  // compute the bounds and extends of the input data
  vtkm::Bounds coordsBounds = inputData.GetCoordinateSystem().GetBounds();

  // setup a camera and point it to towards the center of the input data
  vtkm::rendering::Camera camera;
  camera.ResetToBounds(coordsBounds);

  vtkm::cont::ColorTable colorTable("inferno");

  // Create a mapper, canvas and view that will be used to render the scene
  vtkm::rendering::Scene scene;
  vtkm::rendering::MapperRayTracer mapper;
  vtkm::rendering::CanvasRayTracer canvas(512, 512);
  vtkm::rendering::Color bg(0.2f, 0.2f, 0.2f, 1.0f);

  // Render an image of the output isosurface
  scene.AddActor(vtkm::rendering::Actor(outputData.GetCellSet(),
        outputData.GetCoordinateSystem(),
        outputData.GetField(fieldName),
        colorTable));
  vtkm::rendering::View3D view(scene, mapper, canvas, camera, bg);
  view.Initialize();
  view.Paint();
  view.SaveAs("demo_output.pnm");

  return 0;
}
